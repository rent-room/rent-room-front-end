import logo from './logo.svg';
import './App.css';
import MainContent from './layout/content';

function App() {
  return (
    <div className="d-flex flex-column min-vh-100">
      <MainContent/>
    </div>
  );
}

export default App;
