import hamburgerMenu from '../../public/sideBar/hamburger.png';
import arrowLeft from '../../public/sideBar/arrowLeft.png';
import arrowRight from '../../public/sideBar/arrowRight.png';
import financialReport from '../../public/sideBar/accounts.png';
import promo from '../../public/sideBar/discount.png';
import room from '../../public/sideBar/hotel.png';
import reserve from '../../public/sideBar/reception.png';
import dashboard from '../../public/sideBar/business-report.png';
import facility from '../../public/sideBar/facility.png';



export{
    hamburgerMenu,
    arrowLeft,
    arrowRight,
    financialReport,
    promo,
    room,
    reserve,
    dashboard,
    facility
} 