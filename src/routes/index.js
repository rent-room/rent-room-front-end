import { Routes, Route } from "react-router-dom";
import AdminSidebar from "../layout/adminSideBar";
import AboutPage from "../pages/about";
import DashboardAdmin from "../pages/dahboardAdmin";
import FinancialReport from "../pages/financialReport";
import HomePage from "../pages/home";
import LoginPage from "../pages/login";
import ManagePromo from "../pages/managePromo";
import ManageRoom from "../pages/manageRoom";
import ProfilePage from "../pages/profile";
import RegisterPage from "../pages/register";
import ReserveConfirm from "../pages/reserve";
import ReserveList from "../pages/reserveList";
import RoomPage from "../pages/room";
import RoomDetail from "../pages/roomDetail";
import TransactionHistory from "../pages/transactionHistory";



const ContentRoutes = () => {
    return(
        <Routes>
            <Route path='/' element={<HomePage/>}/>
            <Route path='/login' element={<LoginPage/>}/>
            <Route path='/register' element={<RegisterPage/>}/>
            <Route path='/about' element={<AboutPage/>}/>
            <Route path='/room' element={<RoomPage/>}/>
            <Route path='/room/:id' element={<RoomDetail/>}/>
            <Route path='/reserve' element={<ReserveConfirm/>}/>
            <Route path='/transaction-history' element={<TransactionHistory/>}/>
            <Route path='/profile' element={<ProfilePage/>}/>
            <Route path='/admin-dashboard' element={<AdminSidebar/>}>
                <Route path='/admin-dashboard/reserve-list' element={<ReserveList/>}/>
                <Route path='/admin-dashboard/manage-room' element={<ManageRoom/>}/>
                <Route path='/admin-dashboard/manage-promo' element={<ManagePromo/>}/>
                <Route path='/admin-dashboard/financial-report' element={<FinancialReport/>}/>
                <Route path='/admin-dashboard' element={<DashboardAdmin/>}/>
            </Route>
        </Routes>
)
}

export default ContentRoutes