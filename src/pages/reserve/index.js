

const ReserveConfirm = () => {
    return(
        <>
            <div className="container py-4" style={{minHeight:'40rem'}}>
                <h2 className="text-center mb-5">RESERVE CONFIRMATION</h2>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="row">
                            <div className="col col-lg-5 mb-4">
                                <label className="form-label">ID No (KTP/SIM)</label>
                                <input type="number" className="form-control" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">First Name</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">Last Name</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">Phone</label>
                                <input type="number" className="form-control" />
                            </div>
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">Email</label>
                                <input type="email" className="form-control" />
                            </div>
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">Check-in</label>
                                <input type="date" className="form-control" />
                            </div>
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">Check-out</label>
                                <input type="date" className="form-control" />
                            </div>
                            <div className="col-lg-5 mb-4">
                                <label className="form-label">Guest</label>
                                <input list="guest" type="number" className="form-control" />
                                <datalist id="guest">
                                    <option value="1"/>
                                    <option value="2"/>
                                    <option value="3"/>
                                    <option value="4"/>
                                    <option value="5"/>
                                </datalist>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="mb-2">
                            Select Payment Method
                        </div>
                        <div className="form-check mb-2">
                        <input className="form-check-input" type="radio" name="exampleRadios" value="option1"/>
                        <label className="form-check-label">
                            ATM
                        </label>
                        </div>
                        <div className="form-check mb-2">
                        <input className="form-check-input" type="radio" name="exampleRadios" value="option1"/>
                        <label className="form-check-label">
                            Virtual Account
                        </label>
                        </div>
                        <div className="form-check mb-2">
                        <input className="form-check-input" type="radio" name="exampleRadios" value="option1"/>
                        <label className="form-check-label">
                            E-Wallet
                        </label>
                        </div>
                    </div>
                </div>
            </div>
            <div className="confirm-order-bg">
                <div className="container">
                    <div className="row justify-content-between py-4">
                        <div className="col-lg-7">
                            <h4>
                                COTTAGE FOR FAMILY 3D 2N - Rp950.000
                            </h4>
                        </div>
                        <div className="col-lg-4">
                            <button className="style-button">Confirm Order</button>
                        </div>
                    </div>
                </div>
            </div>
        
        </>
    )
}

export default ReserveConfirm