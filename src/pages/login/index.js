import { useNavigate } from 'react-router-dom';
import imglogin from '../../public/image/login.webp'

const LoginPage = () => {
    let navigate = useNavigate();

    return (
        <>
            {/* <section className="vh-100"> */}
                <div className="container-fluid" style={{height:'40rem'}}>
                    <div className="row">
                    <div className="col-sm-6 text-black">
                        <div className="d-flex align-items-center h-custom-2 px-5 ms-xl-4 mt-5 pt-5 pt-xl-0 mt-xl-n5">
                            <form style={{width: "30rem"}}>
                                <h3 className="fw-bold mb-3 pb-3" style={{letterSpacing:'1px'}}>Log in</h3>

                                <div className="form-outline mb-4">
                                <label className="form-label" for="form2Example18">Email address</label>
                                <input type="email" id="form2Example18" className="form-control form-control-lg" />
                                </div>

                                <div className="form-outline mb-4">
                                <label className="form-label" for="form2Example28">Password</label>
                                <input type="password" id="form2Example28" className="form-control form-control-lg" />
                                </div>

                                <div className="pt-1 mb-4">
                                <button className="style-button" style={{width:'5rem', height:'3rem', fontSize:'1.4rem'}} type="button">Login</button>
                                </div>

                                {/* <p className="small mb-5 pb-lg-2"><a className="text-muted" href="#!">Forgot password?</a></p> */}
                                <p>Don't have an account? <span className="link-info" style={{cursor:'pointer'}} onClick={()=>{navigate('/register')}}>Register here</span></p>
                            </form>
                        </div>
                    </div>

                    <div className="col-sm-6 px-0 d-none d-sm-block">
                        <img src={imglogin} alt="Login image" className="w-100" style={{objectFit:'cover', height:'45rem'}}/>
                    </div>
                    </div>
                </div>
            {/* </section> */}
        </>
    )
}

export default LoginPage