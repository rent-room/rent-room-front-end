import Table from 'react-bootstrap/Table';

const ManageRoom = () => {
    return(
        <div className='container'>
            <h2 className="text-center mt-4">Manage Room</h2>

            <Table responsive>
            <thead>
            <tr>
                <th>No</th>
                <th>Room Name</th>
                <th>Description</th>
                <th>Adult Guest</th>
                <th>Children Gues</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Family Room</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros.</td>
                <td>3</td>
                <td>1</td>
                <td>Rp900.000</td>
                <td>
                    <button>Edit</button>
                    <button>Delete</button>
                </td>
            </tr>
            <tr>
            <td>2</td>
                <td>Deluxe Room</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros.</td>
                <td>2</td>
                <td>0</td>
                <td>Rp400.000</td>
                <td>
                    <button>Edit</button>
                    <button>Delete</button>
                </td>
            </tr>
            <tr>
            <td>3</td>
                <td>Single Room</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros.</td>
                <td>1</td>
                <td>0</td>
                <td>Rp200.000</td>
                <td>
                    <button>Edit</button>
                    <button>Delete</button>
                </td>
            </tr>
            </tbody>
            </Table>
       </div>
    )
}

export default ManageRoom