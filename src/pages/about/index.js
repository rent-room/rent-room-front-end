
import logo from '../../public/image/logo.png'
import image1 from '../../public/jumbotron/jumbo1.webp'
import image2 from '../../public/jumbotron/jumbo2.webp'
import image3 from '../../public/jumbotron/jumbo5.webp'
import image4 from '../../public/jumbotron/jumbo4.webp'

const AboutPage = () => {
    return(
        <div className="container py-5">
            <div className="row mb-5">
                <h3>About</h3>
                <div className="row">
                    <div className="col col-lg-6 lh-lg order-last order-lg-first">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros. Duis vel eros vel nunc tempus maximus lobortis a ante. Fusce feugiat erat at ligula consequat, nec cursus sapien dignissim. Donec lobortis justo congue nulla congue, sed laoreet nisi porta. Maecenas ac aliquam neque, nec ullamcorper eros. Cras interdum tristique enim in fringilla. Donec vehicula ligula sit amet nisl bibendum, vel efficitur felis vestibulum.
                    </div>
                    <div className="col col-lg-6 order-first order-lg-last text-center align-self-center">
                        <img src={logo} />
                    </div>

                </div>
            </div>

            <div className="row">
                <h3 className="text-end">Facility</h3>
                <div className="row">
                    <div className="col col-lg-6 text-center">
                        <img src={image1} className='m-1' style={{width:'15rem', height:'15rem', objectFit:'cover'}} />
                        <img src={image2} className='m-1' style={{width:'15rem', height:'15rem', objectFit:'cover'}} />
                        <img src={image3} className='m-1' style={{width:'15rem', height:'15rem', objectFit:'cover'}} />
                        <img src={image4} className='m-1' style={{width:'15rem', height:'15rem', objectFit:'cover'}} />
                    </div>
                    <div className="col col-lg-6">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros. Duis vel eros vel nunc tempus maximus lobortis a ante. Fusce feugiat erat at ligula consequat, nec cursus sapien dignissim. Donec lobortis justo congue nulla congue, sed laoreet nisi porta. Maecenas ac aliquam neque, nec ullamcorper eros. Cras interdum tristique enim in fringilla. Donec vehicula ligula sit amet nisl bibendum, vel efficitur felis vestibulum.
                    <ul>
                        <li>Front desk</li>
                        <li>24-hour Receptionist</li>
                        <li>Cable TV</li>
                        <li>Shower</li>
                        <li>Parking</li>
                        <li>Free WiFi</li>
                        <li>AC</li>
                        <li>Non-smoking room</li>
                        <li>Swimming Pool</li>
                        <li>Bar & Cafe</li>
                    </ul>
                    </div>

                </div>
            </div>

        </div>
    )
    
}

export default AboutPage