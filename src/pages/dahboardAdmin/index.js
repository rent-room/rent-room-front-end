import Table from 'react-bootstrap/Table';

const DashboardAdmin = () => {
    return(
        <div className="p-4">
            <div className="text-end mb-2">
                <span>Monday, 15-06-2022</span>
            </div>
            <div className="row shadow mb-5" style={{minHeight:'6rem'}}>
                <div className="col-6 border" >
                    <div>
                        Total Guest This Month
                    </div>
                    <h1 className='text-center'>
                        40
                    </h1>
                </div>
                <div className="col-6 border" >
                    <div>
                        Income This Month
                    </div>
                    <h1 className='text-center'>
                        Rp30.000.000
                    </h1>
                </div>
            </div>
            <h4 className='text-center'>Book List</h4>
            <Table responsive>
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Room</th>
                <th>Payment Status</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Total Guest</th>
                <th>Check-In</th>
                <th>Check-Out</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>John</td>
                <td>Family Room</td>
                <td>Paid</td>
                <td>08123123123</td>
                <td>John@email.com</td>
                <td>4</td>
                <td>15-06-2022</td>
                <td>17-06-2022</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Doe</td>
                <td>Deluxe</td>
                <td>Paid</td>
                <td>08123123123</td>
                <td>doe@email.com</td>
                <td>2</td>
                <td>15-06-2022</td>
                <td>17-06-2022</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Susan</td>
                <td>Family 3D2N</td>
                <td>Unpaid</td>
                <td>08123123123</td>
                <td>susan@email.com</td>
                <td>5</td>
                <td>15-06-2022</td>
                <td>17-06-2022</td>
            </tr>
            </tbody>
            </Table>


        </div>
    )
}

export default DashboardAdmin