import Table from 'react-bootstrap/Table';


const ReserveList = () => {
    return(
       <div className='container'>
            <h2 className="text-center mt-4">Reserve List</h2>
            {/* <button>Search & Sort</button> */}
            {/* <div className='border my-4' style={{minHeight:'5rem'}}>
                <div>Search</div>
                <div className='row'>
                    <div className='col-5'>
                        <label>Name</label>
                        <input/>
                        <br/>
                        <label>Room</label>
                        <input/>
                        <br/>
                        <label>Payment status</label>
                        <input/>
                        <br/>
                        <label>Phone</label>
                        <input/>
                    </div>

                    <div className='col-5'>
                        <label>Email</label>
                        <input/>
                        <br/>
                        <label>Check-in</label>
                        <input/>
                        <br/>
                        <label>Check-out</label>
                        <input/>
                    </div>
                </div>    
            </div> */}

            <Table responsive>
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Room</th>
                <th>Payment Status</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Total Guest</th>
                <th>Check-In</th>
                <th>Check-Out</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>John</td>
                <td>Family Room</td>
                <td>Paid</td>
                <td>08123123123</td>
                <td>John@email.com</td>
                <td>4</td>
                <td>15-06-2022</td>
                <td>17-06-2022</td>
                <td>
                    <button>See Details</button>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Doe</td>
                <td>Deluxe</td>
                <td>Paid</td>
                <td>08123123123</td>
                <td>doe@email.com</td>
                <td>2</td>
                <td>15-06-2022</td>
                <td>17-06-2022</td>
                <td>
                    <button>See Details</button>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Susan</td>
                <td>Family 3D2N</td>
                <td>Unpaid</td>
                <td>08123123123</td>
                <td>susan@email.com</td>
                <td>5</td>
                <td>15-06-2022</td>
                <td>17-06-2022</td>
                <td>
                    <button>See Details</button>
                </td>
            </tr>
            </tbody>
            </Table>
       </div>
    )
}

export default ReserveList