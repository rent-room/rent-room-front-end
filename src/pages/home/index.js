import CarouselHome from "./carousel";
import jumbo1 from "../../public/jumbotron/jumbo1.webp";
import jumbo2 from "../../public/jumbotron/jumbo2.webp";
import jumbo3 from "../../public/jumbotron/jumbo3.webp";
import jumbo4 from "../../public/jumbotron/jumbo4.webp";
import jumbo5 from "../../public/jumbotron/jumbo5.webp";
import { useNavigate } from 'react-router-dom';


const HomePage = () => {
    let navigate = useNavigate();


    return(
        <>
            <CarouselHome/>
            <div className="container my-5">
                <h2 className='text-center'>HOT PROMO</h2>
                <div className='row'>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo1} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Family Room 2D 1N</h4>
                                <h5>Rp300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                                <div>- Parking</div>
                                <div>- Wifi</div>
                            </div> */}
                            <button className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo2} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Meeting Package</h4>
                                <h5>Rp1.300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                
                                <div>- Breakfast</div>
                                <div>- Parking</div>
                                <div>- Wifi</div>
                            </div> */}
                            <button className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo3} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Double Room 2D 1N</h4>
                                <h5>Rp150.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                                <div>- Parking</div>
                                <div>- Wifi</div>
                            </div> */}
                            <button className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo5} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Family Room 2D 1N Free Barbeque</h4>
                                <h5>Rp300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                            </div> */}
                            <button onClick={()=>{navigate('/room/1')}} className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    {/* <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'29rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo5} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'4.5rem'}}>
                                <h4>Family Room 2D 1N</h4>
                                <h5>Rp300.000</h5>
                            </div>
                            <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                            </div>
                            <button className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div> */}

                   

                    
                </div>
            </div>
        </>
    )
}

export default HomePage