
import { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import jumbo1 from '../../public/jumbotron/jumbo1.webp';
import jumbo2 from '../../public/jumbotron/jumbo2.webp';
import jumbo3 from '../../public/jumbotron/jumbo3.webp';
import jumbo4 from '../../public/jumbotron/jumbo4.webp';
import jumbo5 from '../../public/jumbotron/jumbo5.webp';


const CarouselHome = () => {
    const [image, ] = useState([jumbo1, jumbo2, jumbo3, jumbo4, jumbo5])

    return(
        <>
            <Carousel variant="dark">
                    {
                        image.map((item, index)=>{
                            return(
                                <Carousel.Item key={index}>
                                    <img
                                    className="d-block w-100"
                                    src={item}
                                    alt={"Slide"+(index+1)}
                                    style={{objectFit:'cover', height:'30rem'}}
                                    />
                                </Carousel.Item>
                            )
                        })
                    }
                </Carousel>

        </>
    )
}

export default CarouselHome