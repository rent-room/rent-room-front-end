import { useNavigate } from 'react-router-dom';
import imgregister from '../../public/image/register.webp'


const RegisterPage = () => {
    let navigate = useNavigate();

    return(
        <div className="container-fluid" style={{height:'45rem'}}>
                <div className="row">
                <div className="col-sm-6 px-0 d-none d-sm-block">
                    <img src={imgregister} alt="Register image" className="w-100" style={{objectFit:'cover', height:'45rem'}}/>
                </div>

                <div className="col-sm-6 text-black">
                    <div className="d-flex align-items-center h-custom-2 px-5 ms-xl-4 mt-5 pt-5 pt-xl-0 mt-xl-n5">
                        <form style={{width: "30rem"}}>
                            <h3 className="fw-bold mb-3 pb-3" style={{letterSpacing:'1px'}}>Register</h3>

                            <div className="form-outline mb-4">
                            <label className="form-label">Username</label>
                            <input type="text" className="form-control form-control-lg" />
                            </div>

                            <div className="form-outline mb-4">
                            <label className="form-label">Email</label>
                            <input type="email" className="form-control form-control-lg" />
                            </div>

                            <div className="form-outline mb-4">
                            <label className="form-label">Password</label>
                            <input type="password" className="form-control form-control-lg" />
                            </div>

                            <div className="form-outline mb-4">
                            <label className="form-label">Repeat Password</label>
                            <input type="password" className="form-control form-control-lg" />
                            </div>

                            <div className="pt-1 mb-4">
                            <button className="style-button" style={{width:'5rem', height:'3rem', fontSize:'1.4rem', float:'right'}} type="button">Submit</button>
                            </div>

                            {/* <p className="small mb-5 pb-lg-2"><a className="text-muted" href="#!">Forgot password?</a></p> */}
                            <p>Have already an account? <span className="link-info" style={{cursor:'pointer'}} onClick={()=>{navigate('/login')}}>Login here</span></p>
                        </form>
                    </div>
                </div>
                </div>
            </div>
    )
}

export default RegisterPage