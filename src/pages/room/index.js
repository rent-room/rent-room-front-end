

import { useNavigate } from 'react-router-dom';
import jumbo1 from "../../public/jumbotron/jumbo1.webp";
import jumbo2 from "../../public/jumbotron/jumbo2.webp";
import jumbo3 from "../../public/jumbotron/jumbo3.webp";
import jumbo4 from "../../public/jumbotron/jumbo4.webp";
import jumbo5 from "../../public/jumbotron/jumbo5.webp";

const RoomPage = () => {
    let navigate = useNavigate();


    const handleChange = () => {
        
    }

    return(
        <div className="container py-4">
            {/* <div className="row mb-3"> */}
                <div className='mx-1 row mb-3 justify-content-center align-items-center background-search-custom'>
                    <div className='row mt-4'>
                        <h5>Search Available Room</h5>
                    </div>
                    <div className='row mb-3'>
                        <div className="col-lg mb-3">
                            <label>Check-in</label>
                            <input  onChange={handleChange} type="date" name="product_name" placeholder="check-in" className="form-control form-control" />
                        </div>
                        <div className="col-lg mb-3">
                            <label>Duration (day)</label>
                            <input  onChange={handleChange} type="number" name="product_name" placeholder="duration" className="form-control form-control" />
                        </div>
                        <div className="col-lg mb-3">
                            <label>Check-out</label>
                            <input  onChange={handleChange} type="date" name="product_name" placeholder="check-out" className="form-control form-control" />
                        </div>
                        <div className="col-lg mb-3">
                            <label>Adults</label>
                            <select  onChange={handleChange} name="category_id" className="select form-control">
                                <option  value={0}>Select</option>
                                <option  value={0}>1</option>
                                <option  value={0}>2</option>
                                <option  value={0}>3</option>
                                <option  value={0}>4</option>
                                <option  value={0}>5</option>
                            </select>
                        </div>
                        <div className="col-lg mb-3">
                            <label>Children</label>
                            <select  onChange={handleChange} name="sort_price" className="select form-control">
                                <option  value={0}>Select</option>
                                <option  value={0}>1</option>
                                <option  value={0}>2</option>
                                <option  value={0}>3</option>
                                <option  value={0}>4</option>
                                <option  value={0}>5</option>
                            </select>
                        </div>
                    </div>
                </div>

            {/* </div> */}
            <div className="row">
            <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo1} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Family Room 2D 1N</h4>
                                <h5>Rp300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                                <div>- Parking</div>
                                <div>- Wifi</div>
                            </div> */}
                            <button onClick={()=>{navigate('/room/1')}} className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo2} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Meeting Package</h4>
                                <h5>Rp1.300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                
                                <div>- Breakfast</div>
                                <div>- Parking</div>
                                <div>- Wifi</div>
                            </div> */}
                            <button className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo3} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Double Room 2D 1N</h4>
                                <h5>Rp150.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                                <div>- Parking</div>
                                <div>- Wifi</div>
                            </div> */}
                            <button className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo5} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Family Room 2D 1N Free Barbeque</h4>
                                <h5>Rp300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                            </div> */}
                            <button onClick={()=>{navigate('/room/1')}} className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                    <div className='col-lg-3 mt-4'>
                        <div className="shadow border w-100" style={{height:'25rem', borderRadius:'10px'}}>
                        <img style={{height:"15rem", objectFit:'contain', cursor:'pointer'}} src={jumbo4} className="card-img-top" />
                        <div className="px-2">
                            <div style={{height:'5.5rem'}}>
                                <h4>Family Room 2D 1N Free Barbeque</h4>
                                <h5>Rp300.000</h5>
                            </div>
                            {/* <div style={{height:'6rem'}}>
                                <div>- Max 4 people</div>
                                <div>- Breakfast</div>
                            </div> */}
                            <button onClick={()=>{navigate('/room/1')}} className="style-button mt-2">See Detail</button>
                        </div>
                        </div>
                    </div>

                   
            </div>
        </div>
    )
}

export default RoomPage