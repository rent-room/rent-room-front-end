import { useParams, useNavigate } from "react-router-dom"


import jumbo1 from '../../public/jumbotron/jumbo1.webp'
import jumbo2 from '../../public/jumbotron/jumbo2.webp'
import jumbo3 from '../../public/jumbotron/jumbo3.webp'
import jumbo5 from '../../public/jumbotron/jumbo5.webp'


const RoomDetail = () => {
    let navigate = useNavigate();
    let {id} = useParams()

    return(
       <div className="container py-5">
            <h2 className="text-center mb-4">Room Family</h2>
            <div className="row mb-4">
                <div className="col-lg ">
                    <div className="mb-1">
                        <img src={jumbo1} style={{width:'100%', height:'25rem', objectFit:'cover'}}/>
                    </div>
                    <div>
                        <img className="me-1" src={jumbo5} style={{width:'10rem', height:'10rem', objectFit:'cover'}}/>
                        <img className="me-1" src={jumbo2} style={{width:'10rem', height:'10rem', objectFit:'cover'}}/>
                        <img className="me-1" src={jumbo3} style={{width:'10rem', height:'10rem', objectFit:'cover'}}/>
                    </div>
                </div>
                <div className="col-lg ">
                    <div className="mb-3">
                        <h4>Rp350.000</h4>
                        <button className="style-button" onClick={()=>{navigate('/reserve')}}>Book A Room</button>
                    </div>
                    <div className="mb-3">
                        <h5>Description</h5>
                        <div>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros. Duis vel eros vel nunc tempus maximus lobortis a ante. Fusce feugiat erat at ligula consequat, nec cursus sapien dignissim. Donec lobortis justo congue nulla congue, sed laoreet nisi porta. Maecenas ac aliquam neque, nec ullamcorper eros. Cras interdum tristique enim in fringilla. Donec vehicula ligula sit amet nisl bibendum, vel efficitur felis vestibulum.
                        </div>
                    </div>
                    <div>
                        <textarea disabled value='Facilities
                        - AC
                        - Parking
                        - Cable TV
                        - Wifi
                        ' className="text-area-custom"/>
                    </div>
                </div>
            </div>
            {/* <div className="row mb-4">
                <h5>Description</h5>
                <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus leo quam, gravida ac egestas non, viverra in eros. Duis vel eros vel nunc tempus maximus lobortis a ante. Fusce feugiat erat at ligula consequat, nec cursus sapien dignissim. Donec lobortis justo congue nulla congue, sed laoreet nisi porta. Maecenas ac aliquam neque, nec ullamcorper eros. Cras interdum tristique enim in fringilla. Donec vehicula ligula sit amet nisl bibendum, vel efficitur felis vestibulum.
                </div>
            </div> */}
            <div className="row mb-4">
                <h5>Review</h5>
                <div className="row shadow border my-3">
                    <div className=" col py-4">
                        <div className="row justify-content-between px-3">
                            <div className="col-9">John - *****</div>
                            <div className="col-3">
                                <div className="row justify-content-end">
                                10/06/2022
                                </div>    
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="px-4">Good Mantap</div>
                        </div>
                    </div>
                </div>
                
                <div className="row border shadow my-3">
                    <div className=" col py-4">
                        <div className="row justify-content-between px-3">
                            <div className="col-9">Doe - *****</div>
                            <div className="col-3">
                                <div className="row justify-content-end">
                                10/06/2022
                                </div>    
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="px-4">Good Mantap</div>
                        </div>
                    </div>
                </div>

            </div>
       </div>
    )
}

export default RoomDetail