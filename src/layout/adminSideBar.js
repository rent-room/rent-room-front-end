import { useState } from 'react';
import Nav from 'react-bootstrap/Nav'
import { useNavigate, Outlet } from 'react-router-dom';
import { arrowLeft, arrowRight, dashboard, facility, financialReport, hamburgerMenu, promo, reserve, room } from '../helper/image';


const AdminSidebar = () => {
    let navigate = useNavigate();
    const [activeLink, setActiveLink] = useState("")
    const [show, setShow] = useState(true)
    const [widhth, setWidth] = useState('')

    return(
        <div style={{minHeight:'80vh'}} className='d-flex'>
                    {
                        show ?
                        <div className='d-lg-block d-none'>
                            <div className="d-flex flex-column p-3 sidebar-custom">
                                <div className="d-flex align-items-center mb-3 mb-md-0 me-md-auto">
                                    <span style={{cursor:'pointer'}} onClick={()=>{navigate("/admin-dashboard");setActiveLink('')}} className="fs-4">Admin Dashboard</span>
                                </div>
                                <hr/>
                                <Nav className='mb-auto'>
                                    <Nav.Link onClick={()=>{navigate("/admin-dashboard/reserve-list");setActiveLink('reserve-list')}}>
                                        <img src={reserve} width="20px" className='me-2'/>
                                        <span className='style-link-nav' style={{color:activeLink === 'reserve-list'&&"#61A9D2"}}>Reserve List</span>
                                    </Nav.Link>
                                    <Nav.Link onClick={()=>{navigate("/admin-dashboard/manage-room");setActiveLink('manage-room')}}>
                                        <img src={room} width="20px" className='me-2'/>
                                        <span className='style-link-nav' style={{color:activeLink === 'manage-room'&&"#61A9D2"}}>Manage Room</span>
                                    </Nav.Link>
                                    <Nav.Link onClick={()=>{navigate("/admin-dashboard/manage-promo");setActiveLink('manage-promo')}}>
                                        <img src={promo} width="20px" className='me-2'/>
                                        <span className='style-link-nav' style={{color:activeLink === 'manage-promo'&&"#61A9D2"}}>Manage Promo</span>
                                    </Nav.Link>
                                    <Nav.Link onClick={()=>{navigate("/admin-dashboard/financial-report");setActiveLink('manage-facility')}}>
                                        <img src={facility} width="20px" className='me-2'/>
                                        <span className='style-link-nav' style={{color:activeLink === 'manage-facility'&&"#61A9D2"}}>Manage Facility</span>
                                    </Nav.Link>
                                    <Nav.Link onClick={()=>{navigate("/admin-dashboard/financial-report");setActiveLink('financial-report')}}>
                                        <img src={financialReport} width="20px" className='me-2'/>
                                        <span className='style-link-nav' style={{color:activeLink === 'financial-report'&&"#61A9D2"}}>Financial Report</span>
                                    </Nav.Link>
                                </Nav>
                                <div className='text-end'>
                                    <span onClick={()=>{setShow(false)}}>
                                        <img style={{cursor:'pointer'}} src={arrowLeft}/>
                                    </span>
                                </div>
                            </div>
                        </div>
                        :
                        <div className={show&&'d-block'}>
                            <div className="d-flex flex-column sidebar-custom-hide">
                                <div className="d-flex pt-3 ps-2 align-items-center mb-3 mb-md-0 me-md-auto">
                                    <span style={{cursor:'pointer'}} onClick={()=>{navigate("/admin-dashboard");setActiveLink('')}} className="fs-4">
                                        <img src={dashboard} width="30px"/>
                                    </span>
                                </div>
                                <hr/>
                                <Nav className='mb-auto'>
                                    <Nav.Link style={{backgroundColor:activeLink === 'reserve-list'&&"white"}} onClick={()=>{navigate("/admin-dashboard/reserve-list");setActiveLink('reserve-list')}}>
                                        <span className='style-link-nav'>
                                            <img src={reserve} width="20px"/>
                                        </span>
                                    </Nav.Link>
                                    <Nav.Link style={{backgroundColor:activeLink === 'manage-room'&&"white"}} onClick={()=>{navigate("/admin-dashboard/manage-room");setActiveLink('manage-room')}}>
                                        <span className='style-link-nav'>
                                            <img src={room} width="20px"/>
                                        </span>
                                    </Nav.Link>
                                    <Nav.Link style={{backgroundColor:activeLink === 'manage-promo'&&"white"}} onClick={()=>{navigate("/admin-dashboard/manage-promo");setActiveLink('manage-promo')}}>
                                        <span className='style-link-nav'>
                                            <img src={promo} width="20px"/>
                                        </span>
                                    </Nav.Link>
                                    <Nav.Link style={{backgroundColor:activeLink === 'manage-facility'&&"white"}} onClick={()=>{navigate("/admin-dashboard/manage-promo");setActiveLink('manage-facility')}}>
                                        <span className='style-link-nav'>
                                            <img src={facility} width="20px"/>
                                        </span>
                                    </Nav.Link>
                                    <Nav.Link style={{backgroundColor:activeLink === 'financial-report'&&"white"}} onClick={()=>{navigate("/admin-dashboard/financial-report");setActiveLink('financial-report')}}>
                                        <span className='style-link-nav'>
                                            <img src={financialReport} width="20px"/>
                                        </span>
                                    </Nav.Link>
                                </Nav>
                                <div className='text-end'>
                                    <span onClick={()=>{setShow(true)}}>
                                        {
                                            widhth!=='small'&&<img style={{cursor:'pointer'}} src={arrowRight}/>
                                        }
                                    </span>
                                </div>
                            </div>
                        </div>
                    }
            <div className='w-100'>
                <div className='d-lg-none d-block'>
                    <span onClick={()=>{setShow(!show);setWidth('small')}}>
                        {/* <img src={hamburgerMenu} width="30px" /> */}
                        <img className='mt-2' src={show ? arrowRight : arrowLeft} width="30px" />
                    </span>
                </div>
                <Outlet/>
            </div>
        </div>
    )
}

export default AdminSidebar