

import fb from './../public/image/facebook.png';
import ig from './../public/image/instagram.png';

const FooterComponent = () => {
    return(
        <div className="background-primary-color py-2 mt-auto">
            <div className='container'>
                <div className="row justify-content-between">
                    <div className="col col-lg-4 align-self-center p-3">
                        <h4>Contact</h4>
                        <div className="lh-lg">Jalan Supratman No 100. Kota Bandung, Jawa Barat</div>
                        <div className="lh-lg">0222151235 / 081213549849</div>
                        <div className="lh-lg">suport@email.com</div>
                        <div>
                            <img width="25rem" className='me-3' src={ig}/>
                            <img width="25rem" src={fb}/>
                        </div>
                    
                    </div>
                    <div className="col col-lg-4 p-3">
                        <img style={{width:'22rem', height:'12rem', objectFit:'cover'}} src='https://www.google.com/maps/d/u/0/thumbnail?mid=1Mby27Aqa5kAKTMm4E374EIhpQRU&hl=en'/>
                    </div>
                </div>
                <div className="text-center">Rent Room - 2022</div>
            </div>
        </div>
    )
}

export default FooterComponent