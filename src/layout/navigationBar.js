
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Container from 'react-bootstrap/Container'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import { useNavigate } from 'react-router-dom';



const NavigationBar = () => {
    let navigate = useNavigate();

    return (
        <Navbar className='background-primary-color' fixed='top' expand="lg">
            <Container>
                <Navbar.Brand onClick={()=>{navigate("/")}} className='fw-bold' style={{color:'#FFFFFF', fontFamily:'poppins', cursor:'pointer'}}>RENT ROOM</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" style={{color: "#E1F9FC", backgroundColor:"#E1F9FC", borderRadius:'10px'}} />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link onClick={()=>{navigate("/")}}>
                            <span  className='style-link-nav'>Home</span>
                        </Nav.Link>
                        <Nav.Link onClick={()=>{navigate("/about")}}>
                            <span className='style-link-nav'>About Us</span>
                        </Nav.Link>
                        <Nav.Link onClick={()=>{navigate("/room")}}>
                            <span className='style-link-nav'>Room</span>
                        </Nav.Link>
                    </Nav>
                    <label>Search Available Room</label>
                    <Form className="d-flex ms-lg-3">
                        <FormControl
                        type="date"
                        placeholder="Select Date"
                        className="me-2"
                        aria-label="Search"
                        />
                        {/* <Button variant="outline-primary">Search</Button> */}
                    </Form>
                    <Nav>
                    <Nav.Link onClick={()=>{navigate("/login")}}>
                        <span  className='style-link-nav'>Login</span>
                    </Nav.Link>
                    <NavDropdown title="Hello User" id="basic-nav-dropdown">
                        <NavDropdown.Item onClick={()=>{navigate("/admin-dashboard")}}>Admin Dashboard</NavDropdown.Item>
                        <NavDropdown.Item onClick={()=>{navigate("/transaction-history")}}>Transaction History</NavDropdown.Item>
                        <NavDropdown.Item onClick={()=>{navigate("/profile")}}>Profile</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item>Log Out</NavDropdown.Item>
                    </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default NavigationBar