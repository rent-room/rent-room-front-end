import { BrowserRouter } from "react-router-dom";
import NavigationBar from "./navigationBar";
import ContentRoutes from "../routes";
import FooterComponent from "./footer";

const MainContent = () => {
    return(
        <BrowserRouter>
            <NavigationBar/>
            <div style={{paddingTop:'50px'}}>
                <ContentRoutes/>
            </div>
            <FooterComponent/>     
        </BrowserRouter>
    )
}

export default MainContent